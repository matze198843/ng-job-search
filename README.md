# NgJobSearch

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

Based on router configuration the App will immediately redirect you to /jobs
path to start with.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io). All tests are to find in src/test within the belonging subfolders to keep them separated from
source code.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page. <br />

<u>Hint:</u> \
The App was deployed with Firebase.
