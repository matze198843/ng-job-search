import { Routes } from "@angular/router";
import { JobListComponent } from "./jobs/job-list/job-list.component";
import { FavoriteListComponent } from "./jobs/favorite-list/favorite-list.component";
import { jobListResolverServiceFn } from "./jobs/job-list/job-list.resolver.service";
import { JobDetailComponent } from "./jobs/job-detail/job-detail.component";
import { jobDetailResolverServiceFn } from "./jobs/job-detail/job-detail.resolver";
import { JobsComponent } from "./jobs/jobs.component";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "/jobs",
    pathMatch: "full",
  },
  {
    path: "",
    component: JobsComponent,
    children: [
      {
        path: "jobs",
        component: JobListComponent,
        resolve: [jobListResolverServiceFn],
      },
      {
        path: "jobs/:id",
        component: JobDetailComponent,
        resolve: [jobDetailResolverServiceFn],
      },
    ],
  },
  {
    path: "favorites",
    component: FavoriteListComponent,
  },
  // Just load in case of really needed which might not happen at all!
  {
    path: "not-found",
    loadComponent: () =>
      import("./not-found/not-found.component").then(
        (m) => m.NotFoundComponent
      ),
  },
  {
    path: "**",
    redirectTo: "/not-found",
  },
];
