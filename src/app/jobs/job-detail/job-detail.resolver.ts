import { ActivatedRouteSnapshot, ResolveFn } from "@angular/router";
import { inject } from "@angular/core";
import {
  JobDetail,
  JobDetailStorageService,
} from "./job-detail-storage.service";
import { JobDetailService } from "./job-detail.service";
import { Observable } from "rxjs";

export const jobDetailResolverServiceFn: ResolveFn<
  JobDetail | Observable<void>
> = (routeSnapshot: ActivatedRouteSnapshot) => {
  const jobDetailStorageService = inject(JobDetailStorageService);
  const jobDetailService = inject(JobDetailService);
  const id = +routeSnapshot.params["id"];

  const found = jobDetailService.getJobDetailById(id);
  if (found.id) return found;

  return jobDetailStorageService.fetchJobDetail(id);
};
