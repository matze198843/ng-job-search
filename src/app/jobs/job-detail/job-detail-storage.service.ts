import { Injectable } from "@angular/core";
import { JobItem } from "../job-list/job-list-storage.service";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, catchError, map, of, tap } from "rxjs";

import { JobDetailService } from "./job-detail.service";
import { Router } from "@angular/router";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

export interface JobDetail extends JobItem {
  location: string;
  industries: string[];
  types: string[];
  description: string | SafeHtml;
  publishDate: Date | string;
}

@Injectable({ providedIn: "root" })
export class JobDetailStorageService {
  constructor(
    private http: HttpClient,
    private jobDetailService: JobDetailService,
    private router: Router,
    private sanatizer: DomSanitizer
  ) {}

  fetchJobDetail(id: number): Observable<void> {
    let jobDetail: JobDetail;
    return this.http.get<JobDetail>(`/jobs/${id}`).pipe(
      map((resp) => {
        jobDetail = {
          id: resp.id,
          companyName: resp.companyName,
          title: resp.title,
          companyLogo: resp.companyLogo,
          reference: resp.reference,
          location: resp.location,
          industries: this.industriesPrettified(resp.industries),
          types: resp.types,
          description: this.sanatizer.bypassSecurityTrustHtml(
            <string>resp.description
          ),
          publishDate: new Date(resp.publishDate),
        };
      }),
      catchError((error: HttpErrorResponse) => {
        // in case of error, return empty Obs$,
        // log to console and navigate to fallback-comp
        console.log(error.message);
        this.router.navigate(["/not-found"]);
        return of();
      }),
      tap(() => {
        this.jobDetailService.addJobDetail(jobDetail);
      })
    );
  }

  private industriesPrettified(industries: string[]): string[] {
    return industries.map((industry) => industry.replaceAll("amp;", ""));
  }
}
