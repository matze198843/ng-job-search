import { Injectable } from "@angular/core";
import { JobDetail } from "./job-detail-storage.service";

@Injectable({ providedIn: "root" })
export class JobDetailService {
  private jobDetails: JobDetail[] = [];

  addJobDetail(detail: JobDetail) {
    this.jobDetails.push(detail);
  }

  getJobDetailById(id: number): JobDetail {
    const found: JobDetail | undefined = this.jobDetails.find(
      (jobDetail) => jobDetail.id === id
    );
    return found ? found : <JobDetail>{};
  }
}
