import { Component, OnInit } from "@angular/core";
import { JobDetail } from "./job-detail-storage.service";
import { JobDetailService } from "./job-detail.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonModule } from "@angular/common";

@Component({
  selector: "app-job-detail",
  standalone: true,
  templateUrl: "./job-detail.component.html",
  imports: [CommonModule],
  styleUrl: "./job-detail.component.css",
})
export class JobDetailComponent implements OnInit {
  jobDetail: JobDetail = <JobDetail>{};
  jobDetails: JobDetail[] = [];

  constructor(
    private jobDetailService: JobDetailService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    const id: number = +this.route.snapshot.params["id"];
    this.jobDetail = this.jobDetailService.getJobDetailById(id);
  }

  onClickBack() {
    this.router.navigate(["../"], { relativeTo: this.route });
  }
}
