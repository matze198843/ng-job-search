import { HttpClient } from "@angular/common/http";
import { Observable, catchError, of, switchMap, tap } from "rxjs";
import { JobListService } from "../job-list/job-list.service";
import { Injectable } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

export interface JobItem {
  id: number;
  companyName: string;
  title: string;
  companyLogo: string | SafeResourceUrl;
  reference: string;
}

@Injectable({ providedIn: "root" })
export class JobListStorageService {
  constructor(
    private http: HttpClient,
    private jobsListService: JobListService,
    private sanitizer: DomSanitizer
  ) {}

  private jobs: JobItem[] = [];

  fetchJobs(): Observable<void> {
    return this.http.get<JobItem[]>("/jobs").pipe(
      switchMap((resp) => {
        return resp.map((jobFromResp) => {
          const safeUrl: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            <string>jobFromResp.companyLogo
          );
          const job: JobItem = {
            id: jobFromResp.id,
            companyName: jobFromResp.companyName,
            title: jobFromResp.title,
            companyLogo: safeUrl,
            reference: jobFromResp.reference,
          };
          this.jobs.push(job);
        });
      }),
      catchError(() => {
        // Initialize with empty arr since its just a mock
        this.jobsListService.setJobs([]);
        return of();
      }),
      tap(() => {
        this.jobsListService.setJobs(this.jobs);
      })
    );
  }
}
