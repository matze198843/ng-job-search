import { ResolveFn } from "@angular/router";
import { inject } from "@angular/core";
import { JobListStorageService } from "./job-list-storage.service";
import { JobListService } from "./job-list.service";
import { JobItem } from "./job-list-storage.service";
import { Observable } from "rxjs";

export const jobListResolverServiceFn: ResolveFn<
  JobItem[] | Observable<void>
> = () => {
  const jobListStorageService = inject(JobListStorageService);
  const jobListService = inject(JobListService);

  if (jobListService.storedJobs.length === 0) {
    return jobListStorageService.fetchJobs();
  } else {
    return jobListService.storedJobs;
  }
};
