import { Component, OnInit } from "@angular/core";
import { JobItem } from "./job-list-storage.service";
import { CommonModule } from "@angular/common";
import { JobListService } from "./job-list.service";
import { JobItemComponent } from "./job-item/job-item.component";
import { RouterOutlet } from "@angular/router";

@Component({
  selector: "app-job-list",
  standalone: true,
  imports: [CommonModule, JobItemComponent, RouterOutlet],
  templateUrl: "./job-list.component.html",
})
export class JobListComponent implements OnInit {
  constructor(private jobListService: JobListService) {}

  jobs: JobItem[] = [];

  ngOnInit(): void {
    this.jobs = this.jobListService.storedJobs;
  }
}
