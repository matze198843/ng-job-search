import { Injectable } from "@angular/core";
import { JobItem } from "./job-list-storage.service";

@Injectable({ providedIn: "root" })
export class JobListService {
  private jobs: JobItem[] = [];

  setJobs(jobs: JobItem[]): void {
    this.jobs = jobs;
  }

  get storedJobs(): JobItem[] {
    return this.jobs;
  }

  getJobById(id: number): JobItem {
    const found: JobItem | undefined = this.jobs.find((job) => job.id === id);
    return found ? found : <JobItem>{};
  }
}
