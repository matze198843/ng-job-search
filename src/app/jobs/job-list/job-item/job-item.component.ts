import { Component, Input } from "@angular/core";
import { JobItem } from "../job-list-storage.service";
import { FavoriteListService } from "../../favorite-list/favorite-list.service";
import { CommonModule } from "@angular/common";
import { Router } from "@angular/router";
import { HighlightDirective } from "../../../shared/highlight.directive";

@Component({
  selector: "app-job-item",
  standalone: true,
  imports: [CommonModule, HighlightDirective],
  templateUrl: "./job-item.component.html",
  styleUrl: "./job-item.component.css",
})
export class JobItemComponent {
  @Input() item: JobItem = {} as JobItem;
  @Input() showStarIcon: boolean = false;

  constructor(
    private favoriteListService: FavoriteListService,
    private router: Router
  ) {}

  onClickFavorite(): void {
    if (this.isMarkedAsFavorite) {
      this.favoriteListService.removeFromFavorites(this.item.id);
    } else {
      this.favoriteListService.addToFavorite(this.item);
    }
  }

  get starId(): string {
    return `star-${this.item.id}`;
  }

  get isMarkedAsFavorite(): boolean {
    return !!this.favoriteListService.storedFavorites.find(
      (fav) => fav.id === this.item.id
    );
  }

  onOpenDetail(): void {
    this.router.navigate(["jobs", `${this.item.id}`]);
  }
}
