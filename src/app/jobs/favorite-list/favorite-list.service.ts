import { JobItem } from "../job-list/job-list-storage.service";
import { Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export class FavoriteListService {
  private favorites: JobItem[] = [];

  addToFavorite(job: JobItem): void {
    this.favorites.push(job);
  }

  removeFromFavorites(id: number): void {
    this.favorites = this.favorites.filter((fav) => fav.id !== id);
  }

  get storedFavorites(): JobItem[] {
    return this.favorites;
  }
}
