import { Component, OnInit } from "@angular/core";
import { JobItem } from "../job-list/job-list-storage.service";
import { FavoriteListService } from "./favorite-list.service";
import { JobItemComponent } from "../job-list/job-item/job-item.component";
import { CommonModule } from "@angular/common";

@Component({
  selector: "app-favorite-list",
  standalone: true,
  imports: [JobItemComponent, CommonModule],
  templateUrl: "./favorite-list.component.html",
})
export class FavoriteListComponent implements OnInit {
  favorites: JobItem[] = [];

  constructor(private favoriteListService: FavoriteListService) {}

  ngOnInit(): void {
    this.favorites = this.favoriteListService.storedFavorites;
  }

  get hasFavorites(): boolean {
    return this.favorites.length > 0;
  }
}
