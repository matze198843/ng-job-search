import { Component } from "@angular/core";
import { RouterOutlet } from "@angular/router";

@Component({
  selector: "app-jobs",
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: "./jobs.component.html",
})
export class JobsComponent {}
