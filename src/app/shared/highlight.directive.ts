import { Directive, ElementRef, HostListener, Input } from "@angular/core";

@Directive({
  selector: "[appHighlight]",
  standalone: true,
})
export class HighlightDirective {
  @Input() marked: boolean = false;
  constructor(private el: ElementRef) {
    this.el.nativeElement.style.cursor = "pointer";
  }

  @HostListener("mouseover")
  onMouseOver() {
    this.el.nativeElement.classList.add("active");
  }

  @HostListener("mouseout")
  onMouseOut() {
    if (!this.marked) {
      this.el.nativeElement.classList.remove("active");
    }
  }
}
