import { Component } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import { HeaderComponent } from "./header/header.component";
import { JobListComponent } from "./jobs/job-list/job-list.component";

@Component({
  selector: "app-root",
  standalone: true,
  imports: [RouterOutlet, JobListComponent, HeaderComponent],
  templateUrl: "./app.component.html",
})
export class AppComponent {}
