import { Component, DebugElement } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { HighlightDirective } from "../../app/shared/highlight.directive";

@Component({
  standalone: true,
  imports: [HighlightDirective],
  template: `
    <a class="btn-group" appHighlight data-test-id="link">Link</a>
    <a class="btn-group" appHighlight [marked]="true" data-test-id="link-marked"
      >Link</a
    >
  `,
})
class HostComponent {}

describe("HighlightDirective", () => {
  let fixture: ComponentFixture<HostComponent>;
  let linkMarked: DebugElement, linkUnmarked: DebugElement;
  let comp: ComponentFixture<HostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HighlightDirective, HostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HostComponent);
    fixture.detectChanges();

    comp = fixture.debugElement.componentInstance;
    linkMarked = fixture.debugElement.query(
      By.css(`[data-test-id="link-marked"]`)
    );
    linkUnmarked = fixture.debugElement.query(By.css(`[data-test-id="link"]`));
  });

  it("curser is set to pointer on default", () => {
    expect(linkMarked.nativeElement.style.cursor).toBe("pointer");
    expect(linkUnmarked.nativeElement.style.cursor).toBe("pointer");
  });

  it('adds class "active" on mouse over event', () => {
    linkMarked.triggerEventHandler("mouseover");
    linkUnmarked.triggerEventHandler("mouseover");
    fixture.detectChanges();

    expect(linkMarked.nativeElement.classList.contains("active")).toBeTruthy();
    expect(
      linkUnmarked.nativeElement.classList.contains("active")
    ).toBeTruthy();
  });

  it('removes class "active" on mouse out event but just for unmarked', () => {
    linkMarked.nativeElement.classList.add("active");
    linkUnmarked.nativeElement.classList.add("active");

    linkMarked.triggerEventHandler("mouseout");
    linkUnmarked.triggerEventHandler("mouseout");
    fixture.detectChanges();

    expect(linkMarked.nativeElement.classList.contains("active")).toBeTruthy();
    expect(linkUnmarked.nativeElement.classList.contains("active")).toBeFalsy();
  });
});
