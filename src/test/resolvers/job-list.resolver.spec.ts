import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { jobListResolverServiceFn } from '../../app/jobs/job-list/job-list.resolver.service';
import { TestBed } from '@angular/core/testing';
import { JobListService } from '../../app/jobs/job-list/job-list.service';
import {
  JobItem,
  JobListStorageService,
} from '../../app/jobs/job-list/job-list-storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { mockedJob } from '../helper';

let mockRoute: ActivatedRouteSnapshot;

let mockSnapshot: jasmine.SpyObj<RouterStateSnapshot>;
let jobListService: JobListService;
let jobListStorageService: JobListStorageService;

let jobListServiceSpy: jasmine.Spy, jobListStorageServiceSpy: jasmine.Spy;

describe('jobListResolverServiceFn', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    mockSnapshot = jasmine.createSpyObj<RouterStateSnapshot>(
      'RouterStateSnapshot',
      ['toString']
    );

    mockRoute = ({
      params: { id: '1' },
    } as unknown) as ActivatedRouteSnapshot;
    jobListService = TestBed.inject(JobListService);
    jobListStorageService = TestBed.inject(JobListStorageService);

    jobListServiceSpy = spyOnProperty(
      jobListService,
      'storedJobs'
    ).and.returnValue([]);
    jobListStorageServiceSpy = spyOn(jobListStorageService, 'fetchJobs');
  });

  it('should call fetchJobs if storedJobs of jobListService is empty and return those from resp', () => {
    TestBed.runInInjectionContext(() =>
      jobListResolverServiceFn(mockRoute, mockSnapshot)
    );

    expect(jobListStorageServiceSpy).toHaveBeenCalled();
  });

  it('should return storedJobs of jobListService if storedJobs is not empty and NOT call fetchJobs', () => {
    jobListServiceSpy.and.returnValue([mockedJob]);

    const result: JobItem[] = <JobItem[]>(
      TestBed.runInInjectionContext(() =>
        jobListResolverServiceFn(mockRoute, mockSnapshot)
      )
    );

    expect(jobListStorageServiceSpy).not.toHaveBeenCalled();
    expect(result).toEqual([mockedJob]);
  });
});
