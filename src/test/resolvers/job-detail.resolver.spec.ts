import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { jobDetailResolverServiceFn } from '../../app/jobs/job-detail/job-detail.resolver';
import { TestBed } from '@angular/core/testing';
import {
  JobDetail,
  JobDetailStorageService,
} from '../../app/jobs/job-detail/job-detail-storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JobDetailService } from '../../app/jobs/job-detail/job-detail.service';
import { mockedJobDetail } from '../helper';

let mockRoute: ActivatedRouteSnapshot;

let mockSnapshot: jasmine.SpyObj<RouterStateSnapshot>;
let jobDetailService: JobDetailService;
let jobDetailStorageService: JobDetailStorageService;

let jobDetailServiceSpy: jasmine.Spy, jobDetailStorageServiceSpy: jasmine.Spy;

describe('jobDetailResolverServiceFn', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    mockSnapshot = jasmine.createSpyObj<RouterStateSnapshot>(
      'RouterStateSnapshot',
      ['toString']
    );

    mockRoute = ({
      params: { id: '1' },
    } as unknown) as ActivatedRouteSnapshot;

    jobDetailService = TestBed.inject(JobDetailService);
    jobDetailStorageService = TestBed.inject(JobDetailStorageService);

    jobDetailServiceSpy = spyOn(
      jobDetailService,
      'getJobDetailById'
    ).and.callThrough();
    jobDetailStorageServiceSpy = spyOn(
      jobDetailStorageService,
      'fetchJobDetail'
    );
  });

  it('calls jobDetailService to get JobDetail by id from route params', () => {
    TestBed.runInInjectionContext(() =>
      jobDetailResolverServiceFn(mockRoute, mockSnapshot)
    );

    expect(jobDetailServiceSpy).toHaveBeenCalledWith(1);
  });

  it('returns JobDetail if found', () => {
    jobDetailServiceSpy.and.returnValue(mockedJobDetail);
    const result: JobDetail = <JobDetail>(
      TestBed.runInInjectionContext(() =>
        jobDetailResolverServiceFn(mockRoute, mockSnapshot)
      )
    );

    expect(result).toEqual(mockedJobDetail);
    expect(jobDetailStorageServiceSpy).not.toHaveBeenCalled();
  });

  it('calls fetchJobDetail if nothing was found', () => {
    <JobDetail>(
      TestBed.runInInjectionContext(() =>
        jobDetailResolverServiceFn(mockRoute, mockSnapshot)
      )
    );

    expect(jobDetailStorageServiceSpy).toHaveBeenCalledWith(1);
  });
});
