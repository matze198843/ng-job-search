import { HttpClient } from '@angular/common/http';
import {
  JobItem,
  JobListStorageService,
} from '../../app/jobs/job-list/job-list-storage.service';
import { JobListService } from '../../app/jobs/job-list/job-list.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { mockedJob, mockedSecondJob } from '../helper';
import { Observable, Subscription, of } from 'rxjs';

describe('JobListStorageService', () => {
  let service: JobListStorageService;

  let sanatizer: DomSanitizer;
  let httpMock: HttpClient;
  let jobListService: JobListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    httpMock = TestBed.inject(HttpClient);
    jobListService = new JobListService();
    sanatizer = {
      bypassSecurityTrustResourceUrl: (value: string): SafeUrl => {
        return value;
      },
    } as DomSanitizer;
    service = new JobListStorageService(httpMock, jobListService, sanatizer);
  });

  describe('fetchJobs', () => {
    let subscription: Subscription = new Subscription();

    it('does the http call and setJobs of jobsListService to store the jobs from resp', () => {
      const httpSpy = spyOn(httpMock, 'get').and.returnValue(
        of([mockedJob, mockedSecondJob])
      );
      const jobsListServiceSpy = spyOn(jobListService, 'setJobs');

      subscription.add(service.fetchJobs().subscribe());
      expect(httpSpy).toHaveBeenCalled();

      expect(jobsListServiceSpy).toHaveBeenCalledWith(<JobItem[]>[
        mockedJob,
        mockedSecondJob,
      ]);
    });

    it('call setJobs of jobsListService with just an empty arr in case of failure', () => {
      const httpSpy = spyOn(httpMock, 'get').and.returnValue(
        new Observable((sub) => sub.error())
      );
      const jobsListServiceSpy = spyOn(jobListService, 'setJobs');

      subscription.add(service.fetchJobs().subscribe());
      expect(httpSpy).toHaveBeenCalled();

      expect(jobsListServiceSpy).toHaveBeenCalledWith(<JobItem[]>[]);
    });

    afterEach(() => {
      subscription.unsubscribe();
    });
  });
});
