import { HttpClient } from "@angular/common/http";
import {
  JobDetail,
  JobDetailStorageService,
} from "../../app/jobs/job-detail/job-detail-storage.service";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { anotherMockedJobDetail } from "../helper";
import { Observable, Subscription, of } from "rxjs";
import { JobDetailService } from "../../app/jobs/job-detail/job-detail.service";
import { Router } from "@angular/router";

describe("JobDetailStorageService", () => {
  let service: JobDetailStorageService;

  let sanatizer: DomSanitizer;
  let httpMock: HttpClient;
  let jobDetailService: JobDetailService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    httpMock = TestBed.inject(HttpClient);
    router = TestBed.inject(Router);
    jobDetailService = new JobDetailService();
    sanatizer = {
      bypassSecurityTrustHtml: (value: string): SafeUrl => {
        return value;
      },
    } as DomSanitizer;
    service = new JobDetailStorageService(
      httpMock,
      jobDetailService,
      router,
      sanatizer
    );
  });

  describe("fetchJobDetail", () => {
    let subscription: Subscription;

    it("does the http call and addJobDetail of jobDetailService to store the jobDetail from resp", () => {
      const httpSpy = spyOn(httpMock, "get").and.returnValue(
        of(anotherMockedJobDetail)
      );
      const jobsDetailServiceSpy = spyOn(jobDetailService, "addJobDetail");

      subscription = service.fetchJobDetail(1).subscribe();
      expect(httpSpy).toHaveBeenCalled();
      expect(jobsDetailServiceSpy).toHaveBeenCalledWith(<JobDetail>{
        ...anotherMockedJobDetail,
        ...{
          publishDate: new Date(anotherMockedJobDetail.publishDate),
          industries: ["Finance & Legal"],
        },
      });
    });

    it("does NOT call addJobDetail of jobDetailService but call router to navigate in case of error", () => {
      const httpSpy = spyOn(httpMock, "get").and.returnValue(
        new Observable((sub) => sub.error({ message: "error" }))
      );
      const jobsDetailServiceSpy = spyOn(jobDetailService, "addJobDetail");
      const routerSpy = spyOn(router, "navigate");

      subscription = service.fetchJobDetail(1).subscribe();
      expect(httpSpy).toHaveBeenCalled();

      expect(jobsDetailServiceSpy).not.toHaveBeenCalled();
      expect(routerSpy).toHaveBeenCalledWith(["/not-found"]);
    });

    afterEach(() => {
      subscription.unsubscribe();
    });
  });
});
