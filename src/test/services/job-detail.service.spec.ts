import { JobDetail } from "../../app/jobs/job-detail/job-detail-storage.service";
import { JobDetailService } from "../../app/jobs/job-detail/job-detail.service";
import { mockedJobDetail } from "../helper";

describe("JobDetailService", () => {
  let service: JobDetailService;

  beforeEach(() => {
    service = new JobDetailService();
  });

  it("addJobDetail adds a JobDetail to service storage", () => {
    service.addJobDetail(mockedJobDetail);
    expect(service["jobDetails"]).toEqual([mockedJobDetail]);

    service.addJobDetail(mockedJobDetail);
    expect(service["jobDetails"]).toEqual([mockedJobDetail, mockedJobDetail]);
  });

  describe("getJobDetailById", () => {
    it("returns JobDetail with requested id if found", () => {
      service["jobDetails"] = [mockedJobDetail];

      expect(service.getJobDetailById(98596)).toEqual(mockedJobDetail);
    });

    it("returns empty JobDetail if nothing was found for requested id", () => {
      service["jobDetails"] = [mockedJobDetail];

      expect(service.getJobDetailById(1)).toEqual(<JobDetail>{});
    });
  });
});
