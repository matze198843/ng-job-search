import { FavoriteListService } from "../../app/jobs/favorite-list/favorite-list.service";
import { mockedJob, mockedSecondJob } from "../helper";

describe("FavoriteListService", () => {
  let service: FavoriteListService;

  beforeEach(() => {
    service = new FavoriteListService();
  });

  it("addToFavorite should add submitted JobItem to service storage", () => {
    service.addToFavorite(mockedJob);
    expect(service["favorites"]).toEqual([mockedJob]);

    service.addToFavorite(mockedSecondJob);
    expect(service["favorites"]).toEqual([mockedJob, mockedSecondJob]);
  });

  describe("removeFromFavorites", () => {
    it("should remove favorite with submitted id from service storage", () => {
      service["favorites"] = [mockedJob, mockedSecondJob];

      service.removeFromFavorites(75278);
      expect(service["favorites"]).toEqual([mockedJob]);

      service.removeFromFavorites(98596);
      expect(service["favorites"]).toEqual([]);
    });

    it("removes nothing if JobItem with submitted id is not in list", () => {
      service["favorites"] = [mockedJob, mockedSecondJob];

      service.removeFromFavorites(1);
      expect(service["favorites"]).toEqual([mockedJob, mockedSecondJob]);
    });

    it("does not run into an error in case of list is empty", () => {
      service.removeFromFavorites(98596);
      expect(service["favorites"]).toEqual([]);
    });
  });

  it("get storedFavorites returns what is in favorites list", () => {
    expect(service.storedFavorites).toEqual([]);

    service["favorites"] = [mockedJob];
    expect(service.storedFavorites).toEqual([mockedJob]);

    service["favorites"] = [mockedJob, mockedSecondJob];
    expect(service.storedFavorites).toEqual([mockedJob, mockedSecondJob]);
  });
});
