import { JobItem } from "../../app/jobs/job-list/job-list-storage.service";
import { JobListService } from "../../app/jobs/job-list/job-list.service";
import { mockedJob, mockedSecondJob } from "../helper";

describe("JobListService", () => {
  let service: JobListService;

  beforeEach(() => {
    service = new JobListService();
  });

  it("setJobs should add all submitted data", () => {
    const mockedPayload: JobItem[] = [mockedJob, mockedSecondJob];
    service.setJobs(mockedPayload);
    expect(service.storedJobs).toEqual(mockedPayload);
  });

  it("get storedJobs always returns the current stored jobs", () => {
    service.setJobs([]);
    expect(service["jobs"]).toEqual([]);

    service.setJobs([mockedJob]);
    expect(service["jobs"]).toEqual([mockedJob]);

    service.setJobs([mockedJob, mockedSecondJob]);
    expect(service["jobs"]).toEqual([mockedJob, mockedSecondJob]);
  });

  describe("getJobById", () => {
    it("returns the job with the requested id", () => {
      service["jobs"] = [mockedJob, mockedSecondJob];
      expect(service.getJobById(98596)).toEqual(mockedJob);
      expect(service.getJobById(75278)).toEqual(mockedSecondJob);
    });

    it("returns empty JobItem if no job was found", () => {
      service["jobs"] = [mockedJob, mockedSecondJob];
      expect(service.getJobById(1)).toEqual(<JobItem>{});
    });
  });
});
