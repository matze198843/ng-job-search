import { ComponentFixture, TestBed } from "@angular/core/testing";
import { JobItemComponent } from "../../app/jobs/job-list/job-item/job-item.component";
import { mockedJob } from "../helper";
import { By } from "@angular/platform-browser";
import { HighlightDirective } from "../../app/shared/highlight.directive";
import { DebugElement } from "@angular/core";

let fixture: ComponentFixture<JobItemComponent>;
let compiled: HTMLElement;
let app: JobItemComponent;
let favoriteListServiceSpy: {
  removeFromFavorites: jasmine.Spy;
  addToFavorite: jasmine.Spy;
};

describe("JobItemComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JobItemComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(JobItemComponent);
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
    app = fixture.componentInstance;
    favoriteListServiceSpy = {
      removeFromFavorites: spyOn(
        app["favoriteListService"],
        "removeFromFavorites"
      ),
      addToFavorite: spyOn(app["favoriteListService"], "addToFavorite"),
    };
  });

  describe("onClickFavorite", () => {
    beforeEach(() => {
      app.item = mockedJob;
    });

    it("should call removeFromFavorites if marked as fav", () => {
      spyOnProperty(app, "isMarkedAsFavorite").and.returnValue(true);
      app.onClickFavorite();
      expect(favoriteListServiceSpy.removeFromFavorites).toHaveBeenCalledWith(
        mockedJob.id
      );
    });

    it("should call addToFavorite if NOT marked as fav", () => {
      spyOnProperty(app, "isMarkedAsFavorite").and.returnValue(false);
      app.onClickFavorite();
      expect(favoriteListServiceSpy.addToFavorite).toHaveBeenCalledWith(
        mockedJob
      );
    });
  });

  it("get starId should return the correct id based on item", () => {
    app.item = mockedJob;
    expect(app.starId).toEqual(`star-${mockedJob.id}`);
  });

  describe("get isMarkedAsFavorite", () => {
    beforeEach(() => {
      app.item = mockedJob;
    });

    it("should return false if not marked", () => {
      spyOnProperty(
        app["favoriteListService"],
        "storedFavorites"
      ).and.returnValue([]);
      expect(app.isMarkedAsFavorite).toBeFalsy();
    });

    it("should return true if marked", () => {
      spyOnProperty(
        app["favoriteListService"],
        "storedFavorites"
      ).and.returnValue([mockedJob]);
      expect(app.isMarkedAsFavorite).toBeTruthy();
    });
  });

  it("onOpenDetail calls navigate from router", () => {
    app.item = mockedJob;
    spyOn(app["router"], "navigate");
    app.onOpenDetail();
    expect(app["router"].navigate).toHaveBeenCalledWith([
      "jobs",
      `${mockedJob.id}`,
    ]);
  });

  it("should img el", () => {
    expect(compiled.querySelector("img")).toBeTruthy();
  });

  describe("render star icon based on condition", () => {
    beforeEach(() => {
      app.item = mockedJob;
    });

    it("true -> should render", () => {
      app.showStarIcon = true;
      fixture.detectChanges();
      expect(compiled.querySelector('[id="star-98596"]')).toBeTruthy();
      expect(
        fixture.debugElement.queryAll(By.directive(HighlightDirective)).length
      ).toBe(1);
    });

    it("highlighting works", () => {
      app.showStarIcon = true;
      fixture.detectChanges();
      const el: DebugElement = fixture.debugElement.queryAll(
        By.directive(HighlightDirective)
      )[0];

      expect(el.nativeElement.classList.contains("active")).toBeFalsy();
      el.triggerEventHandler("mouseover");
      expect(el.nativeElement.classList.contains("active")).toBeTruthy();
      el.triggerEventHandler("mouseout");
      expect(el.nativeElement.classList.contains("active")).toBeFalsy();
    });

    it("false -> should NOT render", () => {
      fixture.detectChanges();
      expect(compiled.querySelector('[id="star-98596"]')).toBeFalsy();
    });
  });

  it("should render the card title", () => {
    app.item = mockedJob;
    fixture.detectChanges();
    expect(compiled.querySelector(".card-title")?.textContent).toEqual(
      ` ${mockedJob.title} `
    );
  });

  it("should render the card text", () => {
    app.item = mockedJob;
    fixture.detectChanges();
    const el: NodeListOf<Element> = compiled.querySelectorAll(".card-text");
    expect(el.length).toBe(2);
    expect(el[0]?.textContent).toEqual(`Company: ${mockedJob.companyName}`);
    expect(el[1]?.textContent).toEqual(`Reference: ${mockedJob.reference}`);
  });
});
