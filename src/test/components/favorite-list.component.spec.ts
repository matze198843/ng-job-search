import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FavoriteListComponent } from "../../app/jobs/favorite-list/favorite-list.component";
import { fakeActivatedRoute, mockedJob } from "../helper";
import { ActivatedRoute } from "@angular/router";

let fixture: ComponentFixture<FavoriteListComponent>;
let compiled: HTMLElement;
let app: FavoriteListComponent;

describe("FavoriteListComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FavoriteListComponent],
      providers: [{ provide: ActivatedRoute, useValue: fakeActivatedRoute }],
    }).compileComponents();

    fixture = TestBed.createComponent(FavoriteListComponent);
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
    app = fixture.componentInstance;
  });

  it("ngOnInit initializes favorites from service", () => {
    const serviceSpy: jasmine.Spy = spyOnProperty(
      app["favoriteListService"],
      "storedFavorites"
    ).and.returnValue([mockedJob]);
    app.ngOnInit();

    expect(serviceSpy).toHaveBeenCalled();
    expect(app.favorites).toEqual([mockedJob]);
  });

  describe("get hasFavorites", () => {
    it("returns true if favorites has at least one entry", () => {
      app.favorites = [mockedJob];
      expect(app.hasFavorites).toBeTruthy();
    });

    it("returns false if favorites is empty", () => {
      expect(app.hasFavorites).toBeFalsy();
    });
  });

  describe("render app-job-item based on items in favorites", () => {
    it("should not render any app-job-item el if no favs are set", () => {
      expect(compiled.querySelector("app-job-item")).toBeFalsy();
    });

    it("should render one app-job-item for each favorite in favorites", () => {
      app.favorites = [mockedJob, mockedJob, mockedJob];
      fixture.detectChanges();
      const appJobItemElements: NodeListOf<Element> = compiled.querySelectorAll(
        "app-job-item"
      );

      expect(appJobItemElements.length).toBe(3);
      expect(compiled.querySelector("h3")).toBeFalsy();
      expect(
        appJobItemElements[0]?.getAttribute("ng-reflect-show-star-icon")
      ).toBeFalsy();
    });
  });

  it("should render fallback element for empty favorites", () => {
    const fallbackEl: HTMLHeadElement | null = compiled.querySelector("h3");
    expect(fallbackEl).toBeTruthy();
    expect(fallbackEl?.textContent).toEqual("There are no favorites selected!");
  });
});
