import { ComponentFixture, TestBed } from "@angular/core/testing";
import { NotFoundComponent } from "../../app/not-found/not-found.component";

let fixture: ComponentFixture<NotFoundComponent>;
let compiled: HTMLElement;

describe("NotFoundComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NotFoundComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NotFoundComponent);
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
  });

  it("should have set the title", () => {
    expect(compiled.querySelector("h2")?.textContent).toEqual(
      "404 Page not found!"
    );
  });

  it("should have the correct paragraph content", () => {
    expect(compiled.querySelector("p")?.textContent).toEqual(
      "The Element you requested was not found."
    );
  });
});
