import { ComponentFixture, TestBed } from "@angular/core/testing";
import { JobDetailComponent } from "../../app/jobs/job-detail/job-detail.component";
import { JobDetail } from "../../app/jobs/job-detail/job-detail-storage.service";
import { ActivatedRoute } from "@angular/router";
import { mockedJobDetail } from "../helper";

let fixture: ComponentFixture<JobDetailComponent>;
let compiled: HTMLElement;
let app: JobDetailComponent;

const fakeActivatedRoute = ({
  snapshot: { params: { id: mockedJobDetail.id } },
} as unknown) as ActivatedRoute;

describe("JobDetailComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JobDetailComponent],
      providers: [{ provide: ActivatedRoute, useValue: fakeActivatedRoute }],
    }).compileComponents();

    fixture = TestBed.createComponent(JobDetailComponent);
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
    app = fixture.componentInstance;
  });

  describe("ngOnInit use id from route params to initialize jobDetail", () => {
    it("empty object if not found by service", () => {
      app.ngOnInit();
      expect(app.jobDetail).toEqual(<JobDetail>{});
    });

    it("sets the right value if found by service", () => {
      const serviceSpy: jasmine.Spy = spyOn(
        app["jobDetailService"],
        "getJobDetailById"
      ).and.returnValue(mockedJobDetail);

      app.ngOnInit();
      expect(serviceSpy).toHaveBeenCalledOnceWith(mockedJobDetail.id);
      expect(app.jobDetail).toEqual(mockedJobDetail);
    });
  });

  it("onClickBack calls router to navigate one step from this route", () => {
    const routerSpy: jasmine.Spy = spyOn(app["router"], "navigate");
    app.onClickBack();
    expect(routerSpy).toHaveBeenCalledWith(["../"], {
      relativeTo: fakeActivatedRoute,
    });
  });

  it("should render button el with onClick listener", () => {
    const onClickSpy: jasmine.Spy = spyOn(app, "onClickBack");
    const button = fixture.debugElement.nativeElement.querySelector("button");
    button.click();

    expect(compiled.querySelector("button")).toBeTruthy();
    expect(onClickSpy).toHaveBeenCalled();
  });

  it("should render img", () => {
    app.jobDetail = mockedJobDetail;
    fixture.detectChanges();
    const el: HTMLImageElement | null = compiled.querySelector("img");
    expect(el).toBeTruthy();
    expect(el?.getAttribute("src")).toEqual(
      mockedJobDetail.companyLogo.toString()
    );
  });

  it("should render title", () => {
    app.jobDetail = mockedJobDetail;
    fixture.detectChanges();
    expect(compiled.querySelector("h1")?.textContent).toEqual(
      `${mockedJobDetail.companyName} - ${mockedJobDetail.title}`
    );
  });

  it('should render "types" and "industries"', () => {
    const mockedDetail: JobDetail = {
      ...mockedJobDetail,
      ...{ industries: ["a", "b"], types: ["a", "b", "c"] },
    };
    app.jobDetail = mockedDetail;
    fixture.detectChanges();
    const allTypes: NodeListOf<Element> = compiled.querySelectorAll(
      '[id^="type-"]'
    );
    const allIndustries: NodeListOf<Element> = compiled.querySelectorAll(
      '[id^="industry-"]'
    );

    expect(allTypes.length).toBe(3);
    expect(allIndustries.length).toBe(2);
  });

  it("should render title", () => {
    app.jobDetail = mockedJobDetail;
    fixture.detectChanges();
    expect(compiled.querySelector("h1")?.textContent).toEqual(
      `${mockedJobDetail.companyName} - ${mockedJobDetail.title}`
    );
  });

  it("should render meta data information", () => {
    app.jobDetail = mockedJobDetail;
    fixture.detectChanges();
    expect(compiled.querySelector('[id="puplish-date"]')?.textContent).toEqual(
      "Publish Date: 2/24/24"
    );
    expect(compiled.querySelector('[id="location"]')?.textContent).toEqual(
      `Location: ${mockedJobDetail.location}`
    );
    expect(compiled.querySelector('[id="reference"]')?.textContent).toEqual(
      `Reference: ${mockedJobDetail.reference}`
    );
  });

  it("should render description innerHTML", () => {
    app.jobDetail = mockedJobDetail;
    fixture.detectChanges();
    expect(
      compiled.querySelector('[id="description"]')?.innerHTML.substring(0, 50)
    ).toEqual(mockedJobDetail.description.toString().substring(0, 50));
  });
});
