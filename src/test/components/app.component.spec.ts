import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AppComponent } from "../../app/app.component";
import { ActivatedRoute } from "@angular/router";
import { fakeActivatedRoute } from "../helper";

let fixture: ComponentFixture<AppComponent>;
let compiled: HTMLElement;
let app: AppComponent;

describe("AppComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppComponent],
      providers: [{ provide: ActivatedRoute, useValue: fakeActivatedRoute }],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    app = fixture.componentInstance;
    compiled = fixture.nativeElement as HTMLElement;
  });

  it("should create the app", () => {
    expect(app).toBeTruthy();
  });

  it("should render app-header", () => {
    expect(compiled.querySelector("app-header")).toBeTruthy();
  });

  it("should render router-outlet", () => {
    expect(compiled.querySelector("router-outlet")).toBeTruthy();
  });
});
