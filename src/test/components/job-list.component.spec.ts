import { ComponentFixture, TestBed } from "@angular/core/testing";
import { JobListComponent } from "../../app/jobs/job-list/job-list.component";
import { fakeActivatedRoute, mockedJob } from "../helper";
import { ActivatedRoute } from "@angular/router";

let fixture: ComponentFixture<JobListComponent>;
let compiled: HTMLElement;
let app: JobListComponent;

describe("JobListComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JobListComponent],
      providers: [{ provide: ActivatedRoute, useValue: fakeActivatedRoute }],
    }).compileComponents();

    fixture = TestBed.createComponent(JobListComponent);
    fixture.detectChanges();
    app = fixture.componentInstance;
    compiled = fixture.nativeElement as HTMLElement;
    spyOnProperty(app["jobListService"], "storedJobs").and.returnValue([
      mockedJob,
    ]);
  });

  it("ngOnInit initializes jobs array from service", () => {
    app.ngOnInit();
    expect(app.jobs).toEqual([mockedJob]);
  });

  it("should render app-job-item for every job with star-icon", () => {
    app.jobs = [mockedJob, mockedJob];
    fixture.detectChanges();
    const el: Element | null = compiled.querySelector("app-job-item");
    expect(compiled.querySelectorAll("app-job-item").length).toBe(2);
    expect(el?.getAttribute("ng-reflect-show-star-icon")).toEqual("true");
  });
});
