import { ComponentFixture, TestBed } from "@angular/core/testing";
import { HeaderComponent } from "../../app/header/header.component";
import { ActivatedRoute } from "@angular/router";
import { fakeActivatedRoute } from "../helper";
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";

let fixture: ComponentFixture<HeaderComponent>;
let compiled: HTMLElement;

describe("HeaderComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HeaderComponent],
      providers: [{ provide: ActivatedRoute, useValue: fakeActivatedRoute }],
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
  });

  it("should render navbar brand title", () => {
    expect(compiled.querySelector(".navbar-brand")?.textContent).toEqual(
      "Find your job"
    );
  });

  it("should render li elements countaining router links", () => {
    const listElements: DebugElement[] = fixture.debugElement.queryAll(
      By.css('li[routerLinkActive="active"]')
    );

    expect(listElements.length).toBe(2);

    expect(listElements[0].query(By.css('a[routerLink="/jobs"]'))).toBeTruthy();
    expect(
      listElements[1].query(By.css('a[routerLink="/favorites"]'))
    ).toBeTruthy();
  });
});
