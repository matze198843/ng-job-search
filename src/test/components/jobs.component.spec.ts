import { ComponentFixture, TestBed } from "@angular/core/testing";
import { JobsComponent } from "../../app/jobs/jobs.component";

let fixture: ComponentFixture<JobsComponent>;
let compiled: HTMLElement;

describe("JobsComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JobsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(JobsComponent);
    fixture.detectChanges();
    compiled = fixture.nativeElement as HTMLElement;
  });

  it("should render router-outlet", () => {
    expect(compiled.querySelector("router-outlet")).toBeTruthy();
  });
});
